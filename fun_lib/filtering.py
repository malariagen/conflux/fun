"""Filter csv by various elements"""

from functools import partial

class CsvListFilter:
    """A callable class which applies filter functions to a datum."""

    def __init__(self, filters):
        """Create a new CsvListFilter using `filters`.

        :param filters: A dict of filter type to filter options.
        """
        self.filters = filters


    def __filter_by_key(self, item, key):
        """Helper method for filtering by dict key"""
        return item.get(key) in self.filters.get(key, [])


    def filter_by_country(self, item):
        """filter by country"""
        return self.__filter_by_key(item, "country")


    def filter_by_plate_id(self, item):
        """filter by plate id"""
        return self.__filter_by_key(item, "plate_id")


    def filter_by_study(self, item):
        """filter by study"""
        return self.__filter_by_key(item, "study")


    def filter_item(self, applicable_filters, item):
        """Return True if `item` passess all `applicable_filters`.

        :param applicable_filters: A list of functions returning a boolean.
        :param item: A dict to check against applicable filters.
        :return: True if `item` passes all filters, False otherwise.
        """
        return all(filter_(item) for filter_ in applicable_filters)


    def __call__(self, input_list):
        """Filter input list according to self.filters.

        Uses filter() to iterate exactly once over input_list, applying the
        approproate self.filter_by_*() methods to each item.

        :param input_list: A list of dicts to filter.
        :return: filter
        """
        applicable_filters = [
            getattr(self, f"filter_by_{filter_name}")
            for filter_name in self.filters
        ]
        filter_all = partial(self.filter_item, applicable_filters)
        return filter(filter_all, input_list)


def parse_filters(filters):
    filter_dict = {}

    if filters is None:
        return filter_dict
        
    for filter in filters:
        if ":" in filter:
            keyvalue = filter.split(':')
            filter_dict[keyvalue[0]] = keyvalue[1]
    return filter_dict


def filter_csv(csv_rows, filters):
    """Given a list of data and a dict of filters, filter the data further.

    :param csv_rows: The list of items to be filtered.
    :param filters: A dict of key:value pairs mapping filter_type:value.
    :return: A filtered verrsion of `csv_rows`, or an unaltered
        `csv_rows` if `filters` is empty..
    """
    filter_func = CsvListFilter(filters)
    return list(filter_func(csv_rows))
