import sys
import enum
from pkg_resources import get_distribution, DistributionNotFound

from fun_lib.csv import csv_to_dict
from fun_lib.argument_parsing import ArgumentParserBuilder
from fun_lib.filtering import parse_filters
from fun_lib.filtering import filter_csv

def __exit_with_error(err):
    print(err,file=sys.stderr)
    sys.exit(-1)

def count(input_file, filters):
    csv = csv_to_dict(input_file)
    if not csv[0]:
        __exit_with_error(csv[1])

    filter_list = parse_filters(filters)
    filtered_csv = filter_csv(csv[1],filter_list)

    print(len(filtered_csv))


def fun_version():
    try:
        return get_distribution("fun").version
    except DistributionNotFound:
        return '<Unknown>'


def main():
    parser = ArgumentParserBuilder.new_instance() \
        .with_count() \
        .with_version(fun_version()) \
        .build()
    arguments = dict(vars(parser.parse_args()))

    if arguments['operation'] == "count":
        count(arguments['input'], arguments['filter'])
    else:
        parser.print_help()
