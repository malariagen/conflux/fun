# fun
A python test for developers

## Requirements

- `>=` Python 3.6
- pytest

## Install
Clone the repository locally and ideally install in a virtual environment:
```bash
# Clone the repo locally
git clone https://gitlab.com/malariagen/conflux/fun
cd fun

# Create and activate a virtual environment
virtualenv venv
source venv/bin/activate

# Install fun
pip install .

# Run the tests
pytest
```

## Uninstall
```bash
pip uninstall fun
```

## Usage
The fun application reads sample csv files and provides filtering.  
Since the application is under development, please see the online help:
```bash
fun -h
```

## Test data
A test csv file is available in the ```test_data``` directory

## License
Fun is free software, licensed under [GPLv3](https://gitlab.com/malariagen/conflux/fun/-/blob/master/LICENSE).
